<?php
    include_once'./includes/functions/inscription-functions.php';
    include_once'./includes/parts/header.php';  
    include_once'./includes/parts/menu-header.php'; 
?>

<main id="container">
<section id="hero-banner">
    <?php
        include_once'./includes/parts/login.php';
    ?>
        <h1>Boutique</h1>
        <!-- Modal HTML embedded directly into document -->
    <div id="lamodal" class="modal">
        <p>Tout achat est lié à la signature d'un contrat</br> de non-responsabilité qui dégage la boutique </br>de toute responsabilité liée à l'utilisation des armes.</p>
        <a href="#" rel="modal:close">J'accepte</a>
        </div>

        <p id="avert">
            <a href="#lamodal" rel="modal:open">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 112.51 38.07">
        <defs>
            <linearGradient id="linear-gradient" x1="20.12" y1="-17.1" x2="91.73" y2="54.51" gradientUnits="userSpaceOnUse">
                <stop offset="0" stop-color="#eaeaea"/><stop offset="0.24" stop-color="#5f5f5f"/>
                <stop offset="0.24" stop-color="#606060"/>
                <stop offset="0.28" stop-color="#919191"/><stop offset="0.33" stop-color="#b8b8b8"/><stop offset="0.37" stop-color="#d3d3d3"/><stop offset="0.4" stop-color="#e4e4e4"/>
                <stop offset="0.43" stop-color="#eaeaea"/><stop offset="0.63" stop-color="#eaeaea"/><stop offset="0.65" stop-color="#e4e4e4"/><stop offset="0.68" stop-color="#d2d2d2"/>
                <stop offset="0.72" stop-color="#b6b6b6"/><stop offset="0.76" stop-color="#8e8e8e"/><stop offset="0.78" stop-color="#767676"/><stop offset="0.87" stop-color="#464545"/>
                <stop offset="1" stop-color="#eaeaea"/></linearGradient><linearGradient id="linear-gradient-2" x1="56.25" y1="0.32" x2="56.25" y2="80.79" xlink:href="#linear-gradient"/>
                <linearGradient id="linear-gradient-3" x1="23.29" y1="-13.92" x2="89.75" y2="52.54" xlink:href="#linear-gradient"/>
                <radialGradient id="radial-gradient" cx="54.98" cy="0.73" r="54.76" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#fff"/><stop offset="0.29" stop-color="#fcfcfc"/><stop offset="0.48" stop-color="#f4f4f4"/><stop offset="0.64" stop-color="#e5e5e5"/>
                    <stop offset="0.78" stop-color="#d0d0d0"/><stop offset="0.91" stop-color="#b5b5b5"/><stop offset="1" stop-color="#9e9e9e"/>
                </radialGradient><linearGradient id="linear-gradient-4" x1="965.13" y1="1490.18" x2="965.13" y2="1515.62" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#010101"/><stop offset="0.17" stop-color="#050505"/><stop offset="0.32" stop-color="#101010"/>
                    <stop offset="0.47" stop-color="#232323"/><stop offset="0.62" stop-color="#3e3e3e"/><stop offset="0.77" stop-color="#606060"/>
                    <stop offset="0.91" stop-color="#8a8a8a"/><stop offset="1" stop-color="#a8a8a8"/>
                </linearGradient><linearGradient id="linear-gradient-5" x1="973.17" y1="1511.97" x2="929.61" y2="1468.41" xlink:href="#linear-gradient-4"/>
                <linearGradient id="linear-gradient-6" x1="943.85" y1="1483.24" x2="1032.93" y2="1569.79" gradientTransform="translate(1.23 1.23)" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#fff"/><stop offset="0.1" stop-color="#fdf9f9"/><stop offset="0.23" stop-color="#f6e9e6"/><stop offset="0.39" stop-color="#ebcfc8"/>
                    <stop offset="0.56" stop-color="#dbaa9f"/><stop offset="0.75" stop-color="#c77b69"/><stop offset="0.95" stop-color="#b04229"/><stop offset="1" stop-color="#a93217"/>
                </linearGradient>
        </defs>
            <g class="cls-71">
            <g id="Calque_1" data-name="Calque 1">
                <rect class="cls-72" width="112.51" height="38.07" rx="5.15"/>
                <rect class="cls-73" x="0.49" y="0.49" width="111.53" height="37.09" rx="4.8"/>
                <rect class="cls-74" x="3.92" y="3.92" width="104.67" height="30.24" rx="3.97"/>
                <rect class="cls-75" x="4.37" y="4.37" width="103.77" height="29.34" rx="3.72"/>
                <text class="cls-76" transform="translate(10.1 23.51) scale(1 1.36)">AVERTISSEMENT !</text>
                <rect class="cls-77" x="3.71" y="4.37" width="103.77" height="29.34" rx="3.72"/>
                <path class="cls-78" d="M1017,1504v-11a3.73,3.73,0,0,0-3.72-3.73H917a3.72,3.72,0,0,0-3.72,3.73v11Z" transform="translate(-908.88 -1484.9)"/>
                <path class="cls-79" d="M916,1516.35a3.26,3.26,0,0,1-.4-1.59V1495a3.37,3.37,0,0,1,3.37-3.37h94.24a3.32,3.32,0,0,1,1.59.4,3.36,3.36,0,0,0-3-1.79H917.55a3.36,3.36,0,0,0-3.36,3.37v19.81A3.37,3.37,0,0,0,916,1516.35Z" transform="translate(-908.88 -1484.9)"/>
                <path class="cls-70" d="M1014.42,1491.64a3.26,3.26,0,0,1,.4,1.59V1513a3.37,3.37,0,0,1-3.37,3.37H917.21a3.43,3.43,0,0,1-1.59-.39,3.38,3.38,0,0,0,3,1.78h94.24a3.37,3.37,0,0,0,3.37-3.37v-19.81A3.38,3.38,0,0,0,1014.42,1491.64Z" transform="translate(-908.88 -1484.9)"/>
        </g></g></svg>
        </a>
    </p>
</section>
<section id="champion" class="flex arme">
    <a href="armes.php">
        <svg class="boutton" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 112.51 38.07">
            <defs>
                <linearGradient id="linear-gradient" x1="20.12" y1="-17.1" x2="91.73" y2="54.51" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#eaeaea"/>
                <stop offset="0.24" stop-color="#5f5f5f"/><stop offset="0.24" stop-color="#606060"/><stop offset="0.28" stop-color="#919191"/><stop offset="0.33" stop-color="#b8b8b8"/>
                <stop offset="0.37" stop-color="#d3d3d3"/><stop offset="0.4" stop-color="#e4e4e4"/><stop offset="0.43" stop-color="#eaeaea"/><stop offset="0.63" stop-color="#eaeaea"/>
                <stop offset="0.65" stop-color="#e4e4e4"/><stop offset="0.68" stop-color="#d2d2d2"/><stop offset="0.72" stop-color="#b6b6b6"/><stop offset="0.76" stop-color="#8e8e8e"/>
                <stop offset="0.78" stop-color="#767676"/><stop offset="0.87" stop-color="#464545"/><stop offset="1" stop-color="#eaeaea"/></linearGradient>
                <linearGradient id="linear-gradient-2" x1="56.25" y1="0.32" x2="56.25" y2="80.79" xlink:href="#linear-gradient"/>
                <linearGradient id="linear-gradient-3" x1="23.29" y1="-13.92" x2="89.75" y2="52.54" xlink:href="#linear-gradient"/>
                <radialGradient id="radial-gradient" cx="55.64" cy="0.73" r="54.76" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff"/><stop offset="0.29" stop-color="#fcfcfc"/>
                <stop offset="0.48" stop-color="#f4f4f4"/><stop offset="0.64" stop-color="#e5e5e5"/><stop offset="0.78" stop-color="#d0d0d0"/><stop offset="0.91" stop-color="#b5b5b5"/>
                <stop offset="1" stop-color="#9e9e9e"/></radialGradient><linearGradient id="linear-gradient-4" x1="1019.26" y1="1290.32" x2="1019.26" y2="1315.75" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#010101"/><stop offset="0.17" stop-color="#050505"/><stop offset="0.32" stop-color="#101010"/><stop offset="0.47" stop-color="#232323"/>
                    <stop offset="0.62" stop-color="#3e3e3e"/><stop offset="0.77" stop-color="#606060"/><stop offset="0.91" stop-color="#8a8a8a"/><stop offset="1" stop-color="#a8a8a8"/>
                </linearGradient><linearGradient id="linear-gradient-5" x1="1027.29" y1="1312.11" x2="983.73" y2="1268.54" xlink:href="#linear-gradient-4"/>
                <linearGradient id="linear-gradient-6" x1="997.98" y1="1283.37" x2="1087.05" y2="1369.92" gradientTransform="translate(1.23 1.23)" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#fff"/><stop offset="0.09" stop-color="#f9f9f9"/><stop offset="0.22" stop-color="#e6e6e6"/><stop offset="0.37" stop-color="#c8c8c8"/>
                    <stop offset="0.53" stop-color="#9f9f9f"/><stop offset="0.71" stop-color="#696969"/><stop offset="0.89" stop-color="#292929"/><stop offset="1" stop-color="#010101"/>
                </linearGradient>
            </defs>
            <g class="cls-81">
                <g id="Calque_1" data-name="Calque 1">
                    <rect class="cls-82" width="112.51" height="38.07" rx="5.15"/>
                    <rect class="cls-83" x="0.49" y="0.49" width="111.53" height="37.09" rx="4.8"/><rect class="cls-84" x="3.92" y="3.92" width="104.67" height="30.24" rx="3.97"/>
                    <rect class="cls-85" x="4.37" y="4.37" width="103.77" height="29.34" rx="3.72"/><text class="cls-86" transform="translate(36.73 23.69) scale(1 1.36)">ARMES</text>
                    <rect class="cls-87" x="4.37" y="4.37" width="103.77" height="29.34" rx="3.72"/><path class="cls-88" d="M1071.14,1304.17v-11.05a3.72,3.72,0,0,0-3.72-3.72H971.09a3.73,3.73,0,0,0-3.72,3.72v11.05Z" transform="translate(-963 -1285.03)"/>
                    <path class="cls-89" d="M970.09,1316.48a3.37,3.37,0,0,1-.4-1.58v-19.81a3.37,3.37,0,0,1,3.37-3.37h94.24a3.38,3.38,0,0,1,1.59.4,3.36,3.36,0,0,0-3-1.78H971.68a3.37,3.37,0,0,0-3.37,3.37v19.8A3.35,3.35,0,0,0,970.09,1316.48Z" transform="translate(-963 -1285.03)"/>
                    <path class="cls-80" d="M1068.55,1291.78a3.36,3.36,0,0,1,.39,1.58v19.81a3.36,3.36,0,0,1-3.36,3.37H971.33a3.37,3.37,0,0,1-1.58-.4,3.35,3.35,0,0,0,3,1.78H1067a3.37,3.37,0,0,0,3.37-3.37v-19.8A3.35,3.35,0,0,0,1068.55,1291.78Z" transform="translate(-963 -1285.03)"/>
                </g>
            </g>
        </svg>
    </a>


    <a href="armures.php">
        <svg class="boutton" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 112.51 38.07">
            <defs>
                <linearGradient id="linear-gradient" x1="20.12" y1="-17.1" x2="91.73" y2="54.51" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#eaeaea"/>
                <stop offset="0.24" stop-color="#5f5f5f"/><stop offset="0.24" stop-color="#606060"/><stop offset="0.28" stop-color="#919191"/><stop offset="0.33" stop-color="#b8b8b8"/>
                <stop offset="0.37" stop-color="#d3d3d3"/><stop offset="0.4" stop-color="#e4e4e4"/><stop offset="0.43" stop-color="#eaeaea"/><stop offset="0.63" stop-color="#eaeaea"/>
                <stop offset="0.65" stop-color="#e4e4e4"/><stop offset="0.68" stop-color="#d2d2d2"/><stop offset="0.72" stop-color="#b6b6b6"/><stop offset="0.76" stop-color="#8e8e8e"/>
                <stop offset="0.78" stop-color="#767676"/><stop offset="0.87" stop-color="#464545"/><stop offset="1" stop-color="#eaeaea"/>
                </linearGradient>
                <linearGradient id="linear-gradient-2" x1="56.25" y1="0.32" x2="56.25" y2="80.79" xlink:href="#linear-gradient"/>
                <linearGradient id="linear-gradient-3" x1="23.29" y1="-13.92" x2="89.75" y2="52.54" xlink:href="#linear-gradient"/>
                <radialGradient id="radial-gradient" cx="55.64" cy="0.73" r="54.76" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#fff"/><stop offset="0.29" stop-color="#fcfcfc"/>
                <stop offset="0.48" stop-color="#f4f4f4"/><stop offset="0.64" stop-color="#e5e5e5"/><stop offset="0.78" stop-color="#d0d0d0"/><stop offset="0.91" stop-color="#b5b5b5"/>
                <stop offset="1" stop-color="#9e9e9e"/></radialGradient><linearGradient id="linear-gradient-4" x1="958.51" y1="1381.46" x2="958.51" y2="1406.9" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#010101"/><stop offset="0.17" stop-color="#050505"/><stop offset="0.32" stop-color="#101010"/><stop offset="0.47" stop-color="#232323"/>
                    <stop offset="0.62" stop-color="#3e3e3e"/><stop offset="0.77" stop-color="#606060"/><stop offset="0.91" stop-color="#8a8a8a"/><stop offset="1" stop-color="#a8a8a8"/>
                </linearGradient><linearGradient id="linear-gradient-5" x1="966.55" y1="1403.25" x2="922.99" y2="1359.69" xlink:href="#linear-gradient-4"/>
                <linearGradient id="linear-gradient-6" x1="937.23" y1="1374.52" x2="1026.31" y2="1461.06" gradientTransform="translate(1.23 1.23)" gradientUnits="userSpaceOnUse">
                    <stop offset="0" stop-color="#fff"/><stop offset="0.09" stop-color="#f9fbfa"/><stop offset="0.22" stop-color="#e6f1ea"/><stop offset="0.38" stop-color="#c8e1d1"/>
                    <stop offset="0.55" stop-color="#9fcaae"/><stop offset="0.73" stop-color="#69ad81"/><stop offset="0.93" stop-color="#298a4b"/><stop offset="1" stop-color="#0e7b34"/>
                </linearGradient>
            </defs>
            <g class="cls-91">
                <g id="Calque_1" data-name="Calque 1">
                    <rect class="cls-92" width="112.51" height="38.07" rx="5.15"/>
                    <rect class="cls-93" x="0.49" y="0.49" width="111.53" height="37.09" rx="4.8"/><rect class="cls-94" x="3.92" y="3.92" width="104.67" height="30.24" rx="3.97"/>
                    <rect class="cls-95" x="4.37" y="4.37" width="103.77" height="29.34" rx="3.72"/><text class="cls-96" transform="translate(31.89 22.82) scale(1 1.36)">ARMURES </text>
                    <rect class="cls-97" x="4.37" y="4.37" width="103.77" height="29.34" rx="3.72"/><path class="cls-98" d="M1010.4,1395.32v-11a3.73,3.73,0,0,0-3.72-3.73H910.35a3.72,3.72,0,0,0-3.72,3.73v11Z" transform="translate(-902.26 -1376.18)"/>
                    <path class="cls-99" d="M909.35,1407.63a3.26,3.26,0,0,1-.4-1.59v-19.8a3.37,3.37,0,0,1,3.37-3.37h94.24a3.43,3.43,0,0,1,1.59.39,3.37,3.37,0,0,0-3-1.78H910.93a3.36,3.36,0,0,0-3.36,3.37v19.81A3.37,3.37,0,0,0,909.35,1407.63Z" transform="translate(-902.26 -1376.18)"/>
                    <path class="cls-90" d="M1007.8,1382.92a3.26,3.26,0,0,1,.4,1.59v19.8a3.37,3.37,0,0,1-3.37,3.37H910.59a3.43,3.43,0,0,1-1.59-.39,3.37,3.37,0,0,0,3,1.78h94.25a3.37,3.37,0,0,0,3.37-3.37v-19.81A3.38,3.38,0,0,0,1007.8,1382.92Z" transform="translate(-902.26 -1376.18)"/>
                </g>
            </g>
        </svg>
    </a>

</section> 

</main>

<?php
    include_once'./includes/parts/footer.php';
?>

