
<?php
  include_once'./includes/parts/header.php';  
  include_once'./includes/parts/menu-header.php';
?>
  
  <main id="container">

    <section id="hero-banner">
      <h1>Survival Phrak</h1>
    </section>

    <?php 
      include_once'./includes/parts/banniere.php';
    ?>

    <section class="container">
      <div>
        <h2>Présentation</h2>
        <p class="txt-white">Phornot est une plateforme d’animation socio culturelle qui offre un espace de jeu de combat en ligne en format individuel.</br></br>
          C’est un nouveau type de jeu de combat. En effet, celui-ci propose un combat à l’aide de tous les membres du corps et armés des combattants. 
          C’est grâce à l’usage de toutes vos capacités que vous pourrez venir à bout de votre adversaire ! Chaque combattant a une personnalité unique. 
          Il peut utiliser différentes armes, chacune ayant des atouts différents.</br></br>
          La survie des habitants de la ville de PHORNOT relève de leur résistance physique assurant leur victoire sur chaque adversaire puisque 
          les combats sont uniques et ne se ressemblent pas.</br></br>
          Les combattants sont évalués sur la durée de leur vie dans la ville. Ils doivent se défendre à tous les prix avec tous les moyens qu’ils disposent. 
          Ainsi, chaque combattant est plus ou moins fort dans chacun de ses domaines et c’est ce qui fait leur différence principale.</br></br>
          Il n’y a pas de match nul ou reporté, ce n’est plus la cour de récréation ! Les combattants vont affronter des adversaires de plus en plus 
          coriaces en fonction de leur aptitude à survivre. À chacun de gagner le plus de matchs afin de rester en vie le plus longtemps. Attention, 
          Les combattants s’affrontent deux par deux en respectant le règlement établit, seul le décès d’un des deux combattants signe l’arrêt de l’épreuve. 
        </p>
          <a class="btn-ghost" href="#">En savoir plus</a>
      </div>
    </section>

  </main>

<?php
  include_once'./includes/parts/footer.php';
?>
  


