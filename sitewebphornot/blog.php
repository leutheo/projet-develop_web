
<?php
  include_once'./includes/parts/header.php';  
  include_once'./includes/parts/menu-header.php';
?>
  
  <main id="container">

    <section id="hero-banner">
      <h2>Blog</h2>
    </section>


    <section class="container">
    <?php
        include_once'./includes/functions/blog_function.php';
      ?>

      <ul>
        <?php while($a = $articles->fetch()){ ?>
          <li class="txt-white">
            <a class="txt-white" href="articles.php?id=<?php echo $a['id'] ?>"><?php echo $a['titre'] ?></a> 
          </li>
        <?php } ?>
      </ul>
      <button class="btn"><a class="txt-white" href="admin.php">Accès administrateur</a></button>
    </section>
  </main>

<?php
  include_once'./includes/parts/footer.php';
?>