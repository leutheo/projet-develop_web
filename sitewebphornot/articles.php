
<?php
  include_once'./includes/parts/header.php';  
  include_once'./includes/parts/menu-header.php';
?>
  
  <main id="container">

    <section id="hero-banner">
      <h2>Articles</h2>
    </section>


    <section class="container">
    <?php
      include_once'./includes/functions/articles-function.php';
    ?>

    <h3 class="txt-white"><?php echo $titre, " | Date de création : ", $date, " | Auteur : " ?></h3>
     <p class="txt-white"><?php echo $contenu ?></p>

     <button class="btn"><a class="txt-white" href="blog.php">Aller sur le blog</a></button>
    </section>
  </main>

<?php
  include_once'./includes/parts/footer.php';
?>