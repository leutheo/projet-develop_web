  <?php
    include_once'./includes/functions/data/connecteur.php';
    include_once'./includes/parts/header.php';  
    include_once'./includes/parts/menu-header.php';
  ?>

<main id="container">
    <section id="hero-banner">
      <?php
        include_once'./includes/parts/login.php';
      ?>
      <h1>Inscrivez-vous ! </br> C'est rapide et facile</h1>
    </section>
    <?php 
      include_once'./includes/functions/creationcompte-function.php';
    ?>

      <section id="formulaire" >
      <h2 class="form">Formulaire d'inscription</h2>
      <form id="form-inscription" action="" method="POST">
        <fieldset>
          <legend>Création de compte</legend>
          <p>
            <label for="nom">Nom :</label>
            <input class="inputLargeur" type="text" name="nom" id="nom" placeholder="nom" value="<?php if(isset($prenom)) {echo $prenom;}?>" >
          </p>
          <p>
            <label for="prenom">Prénom :</label>
            <input class="inputLargeur" type="text" name="prenom" id="prenom" placeholder="prénom" value="<?php if(isset($prenom)) {echo $prenom;}?>">
          </p>
          <p>
            <label for="naissance">Date de naissance :</label>
            <input class="inputLargeur" type="date" name="naissance" id="naissance" value="<?php if(isset($naissance)) {echo $naissance;} ?>">
          </p>
          <p>
            <label for="courriel">Courriel :</label>
            <input class="inputLargeur" type="email" name="courriel" id="courriel" placeholder="example@mail.com" value="<?php if(isset($courriel)) {echo $courriel;}?>">
          </p>
          <p>
            <label for="numero">Téléphone :</label>
            <input class="inputLargeur" type="tel" name="numero" id="numero" placeholder="(000)-000-0000" value="<?php if(isset($numero)) {echo $numero;}?>">
          </p>
          <p>
            <label for="utilisateur">Nom d'utilisateur :</label>
            <input class="inputLargeur" type="text" id="utilisateur" name="utilisateur" placeholder="nom utilisateur" value="<?php if(isset($utilisateur)) {echo $utilisateur;}?>">
          </p>
          <p>
            <label for="psw1">Mot de passe :</label>
            <input class="inputLargeur" type="password" id="pass" name="pass" placeholder="mot de passe"> 
          </p>
          <p>
            <label for="psw2">Confirmation mot de passe :</label>
            <input class="inputLargeur" type="password" id="confirmpass" name="confirmpass" placeholder="confirmez mot de passe" >
          </p>
          <p>
            <label for="psw3modifier">Insérez votre photo :</label>
            <input class="inputLargeur" type="file" name="photo" placeholder="Choisissez votre photo de profile" >
          </p>
        </fieldset>       
        <input class="bouton btn" type="reset" value="Effacer le formulaire" >
        <input class="bouton fr btn" type="submit" name="inscription" value="Soumettre votre formulaire">
      </form>
      <?php
        if(isset($erreur)){
          echo $erreur;  
        } 
      ?>
    </section>
</main>
  <?php
    include_once'./includes/parts/footer.php';
  ?>
 