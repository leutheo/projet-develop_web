<?php
  include_once'./includes/parts/header.php';  
  include_once'./includes/parts/menu-header.php';
?>
  
  <main id="container">
    <section id="hero-banner">
<?php 
  include_once'./includes/parts/login.php';

  if(isset($_COOKIE['connexion'])){
    echo 'Votre nom de connexion est : '.$_COOKIE['connexion'];
  }
?>
    
<?php
  if (isset($_SESSION["id"]) && $_SESSION["id"] > 0) {
    include_once'./includes/functions/profil-functions.php'; 
?> 
<h1>Profil du membre <?php echo $userconnect["nom"]; ?></h1>
</section>

  <section id="champion" class="flex punch">
    <div class="champion-cards edit">
      <?php 
        if (!empty($userconnect["photo"])) {
          $url_photo = $userconnect["photo"];
          echo "<img src='./image/$url_photo'>"; 
        }
      ?>
    </div>
    <div class="champion-cards edit">
      <div>
      <p class="txt-white ">
        Nom : <?php echo $userconnect["nom"]; ?>
      </p>
      <p class="txt-white ">
        Prénom : <?php echo $userconnect["prenom"]; ?>
      </p>
      <p class="txt-white ">
        Date de naissance : <?php echo $userconnect["naissance"]; ?>
      </p>
      <p class="txt-white ">
        Courriel : <?php echo $userconnect["courriel"]; ?>
      </p>
      <p class="txt-white ">
        Téléphone : <?php echo $userconnect["numero"]; ?>
      </p>
      <p class="txt-white ">
        Nom d'utilisateur : <?php echo $userconnect["utilisateur"]; ?>
      </p>
      </div>

      <div class="flex">
<?php 
    if(isset($_SESSION["id"]) && $userconnect["id"] == $_SESSION["id"]) {
?>
      <a href="modifier-profil.php" class="bouton btn btnedit">Éditer le profil</a>
      <a href="deconnexion.php" class="bouton btn">Déconnexion</a>
<?php 
    }
?> 
      </div>
    </div>
  </section>

<section>

<?php
  } else {
  ?>

<div align="center">
  <h3 style="Color: red">Votre session n'est pas ouverte, Veuillez vous connecter ou créer un compte</h3>
  </br>
  <a href="inscription.php" class="btn">S'inscrire</a>
</div>

<?php  
     }
?>

<?php
  if(isset($erreurlogin) && !empty($erreurlogin)){
?>
  <h2>Gestion des erreurs du formulaire</h2>
   <?php
    echo $erreurlogin;   
   ?> 
</section>

</main>

<?php
   }  
?>

<?php
  include_once'./includes/parts/footer.php';
?>

