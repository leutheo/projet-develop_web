<?php
SESSION_START();
include_once'cookie.php';


?>
<!doctype html>
<html class="no-js" lang="fr">

<head>
  <meta charset="utf-8">
  <title>Tournoi Phornot</title>
  <meta name="description" content="Exercice responsive, mobile first">

  <!-- meta viewport : importante à inclure pour le responsive ! -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <link href="https://fonts.googleapis.com/css?family=Dosis:600|Open+Sans:400,700" rel="stylesheet">

  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css">
  <link media="screen" rel="stylesheet" href="css/main.css">
  <link media="screen" rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
</head>

<body>