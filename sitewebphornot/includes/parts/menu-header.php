<header id="header-main" class="clearfix">
    <div id="container" class="container">
      <div id="site-logo" class="logo fl">
        <a href="index.php">
          <img src="./img/logo.png" alt="PHORNOT">
          <!-- <h1>PHORNOT</h1> -->
        </a>
      </div>
      <nav id="main-nav" class="fr">
        <ul>
          <li><a href="index.php">Accueil</a></li>
          <li><a href="champions.php">Champions</a></li>
          <li><a href="profil.php">Profil</a></li>
          <li ><a href="boutique.php">Boutique</a></li>
          <li><a href="blog.php">Blog</a></li>
          <li><a class="btn" href="inscription.php">Login / inscription</a></li>
        </ul>
      </nav>
    </div>
  </header>

<header id="header-mobile" class="clearfix">
  <div id="container" class="container menumobileflex">
    <div id="site-logo" class="logo fl">
      <a href="index.php">
        <img src="./img/logo.png" alt="PHORNOT">
      </a>
    </div>

    <div class="dropdown" id="menusvg">
        <i id="activate_menu" class="fas fa-bars dropbtn"></i>
        <div id="myDropdown" class="dropdown-content menu-bar">
          <a href="index.php">Accueil</a>
          <a href="champions.php">Champions</a>
          <a href="profil.php">Profil</a>
          <a href="blog.php">Blog</a>
          <a href="boutique.php">Boutique</a>
          <a class="btnmenumobile" href="inscription.php">Inscription/Login</a>
        </div>
    </div>
  </div> 
</header>

