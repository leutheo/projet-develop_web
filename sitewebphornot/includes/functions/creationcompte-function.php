<?php
    include_once'./includes/functions/data/connecteur.php';
    
    // Lecture des valeurs du formulaire
    function form_values($nom){
        echo(isset($_POST[$nom]) ? $_POST : "");
    }

    if(isset($_POST['inscription'])){
        // Vérifie que les variables existes et les nettoye
        // $identification = validate_checkbox($_POST["identification"]);
        
        $nom = validate_text_fields($_POST["nom"]);
        $prenom = validate_text_fields($_POST["prenom"]);
        $naissance = validate_text_fields($_POST["naissance"]);
        $courriel = validate_text_fields($_POST["courriel"]);
        $numero = validate_text_fields($_POST["numero"]);
        $utilisateur = validate_text_fields($_POST["utilisateur"]);
        $pass = trim(sha1($_POST["pass"]));
        $confirmpass = trim(sha1($_POST["confirmpass"]));
        $photo = validate_text_fields($_POST["photo"]);
        
        // Vérifie que les champs ne sont pas vides
        if (!empty($_POST["nom"]) && !empty($_POST["prenom"]) && !empty($_POST["naissance"]) && !empty($_POST["courriel"]) 
            && !empty($_POST["numero"])&& !empty($_POST["utilisateur"]) && !empty($_POST["pass"]) && !empty($_POST["confirmpass"]) && !empty($_POST["photo"])) {
            
            $nomlength = strlen($nom);
            $prenomlength = strlen($prenom);
            $courriellength = strlen($courriel);
            $numerolength = strlen($numero);
            $utilisateurlength = strlen($utilisateur);
            $passlength = strlen($pass);
            $confirmpasslength = strlen($confirmpass);
            $photolength = strlen($photo);
                     
            // Vérifie que les champs contiennent moins de 255 caractères
            if ($nomlength <= 255 && $prenom <= 255 && $courriellength <= 255 && $numerolength <= 255 
                && $utilisateurlength <= 255 && $passlength <= 255 && $confirmpasslength <= 255 && $photolength <= 255) {
                    
                    // Valide que le courriel n'est de type text
                    if (courriel_validate($courriel)) {
                        try {
                            // Prépare la bdd pour qu'elle recoive les données
                            $reqcourriel = $bdd->prepare("SELECT * FROM membres WHERE courriel = ?");
                            $reqcourriel->execute(array($courriel));
                            $courrielexiste = $reqcourriel->rowCount();
                        } catch (PDOException $e) {
                            return $e->getMessage();
                        }
                                                
                        // Vérifie que le courriel n'existe pas encore dans la bdd
                        if ($courrielexiste == 0) {

                            // Vérifie que les mdp sont identiques
                            if ($pass == $confirmpass) {
                                try {
                                    // Prépare la bdd pour insérer les données
                                    $insertmbr = $bdd->prepare("INSERT INTO membres(nom, prenom, naissance, courriel, numero, utilisateur, pass, photo, administrateur) VALUES(?, ?, ?, ?, ?, ?, ?, ?, '0')");
                                    $insertmbr->execute(array($nom, $prenom, $naissance, $courriel, $numero, $utilisateur, $pass, $photo));
                                    $erreur = "Votre compte a bien été crée ! <a href=\"inscription.php\">Connexion</a>";
                                } catch (PDOException $e) {
                                    return $e->getMessage();
                                }
                                
                            }else {
                                $erreur = "Vos mots de passe ne correspondent pas !";
                            }
                        }else {
                            $erreur = "Votre adresse courreille est déjà utilisée !";
                        }
                    }else {
                        $erreur = "Votre adresse courrielle n'est pas correct !";
                    }
                
            }else {
                $erreur = "Vos cases à remplir ne doivent pas dépasser 255 caractères !";
            }
        }else {
            $erreur = "Toutes les cases doivent être complétées !";
        }
    }

    // Filtre de validation

    // Filtre le courriel
    function courriel_validate($courriel){
        return filter_var($courriel, FILTER_VALIDATE_EMAIL);
    }

    // Nettoyage des données
    function validate_text_fields($champ){
        return filter_var(trim(htmlspecialchars($champ)), FILTER_SANITIZE_STRING);
    }
?>
