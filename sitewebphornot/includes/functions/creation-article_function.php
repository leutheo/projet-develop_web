<?php
    // Appelle la base des données
    include_once'./includes/functions/data/connecteur.php';

    $mode_edition = 0;

    if (isset($_GET["edit"]) AND !empty($_GET["edit"])) {

        $mode_edition = 1;

        $edit_id = htmlspecialchars($_GET["edit"]);
        
        $edit_article = $bdd->prepare("SELECT * FROM articles WHERE id = ?");
        $edit_article->execute(array($edit_id));

        if ($edit_article->rowCount() == 1) {
            $edit_article = $edit_article->fetch();
        }else {
            die("L\'article que vous recherchez n'existe pas...");
        }
    }


    if (isset($_POST["article_titre"], $_POST["article_contenu"])) {     
        if (!empty($_POST["article_titre"]) && !empty($_POST["article_contenu"])) {
            $article_titre = htmlspecialchars($_POST["article_titre"]);
            $article_contenu = htmlspecialchars($_POST["article_contenu"]);

            if ($mode_edition == 0) {
                $insertion = $bdd->prepare("INSERT INTO articles (titre, contenu, date_time_publication) VALUES (?, ?, NOW())");
                $insertion->execute(array($article_titre, $article_contenu));

                $erreurarticle = "Votre article a bien été posté";

            }else {
                $update = $bdd->prepare('UPDATE articles SET titre = ?, contenu = ?, date_time_edition = NOW() WHERE id = ?');
                $update->execute(array($article_titre, $article_contenu, $edit_id));
                header('Location: articles.php?id='.$edit_id);

                $message = 'Votre article a bien été mis à jour !';
            }
        }else {
            $erreurarticle = "Veuillez remplir tous les champs !";
        }
    }


?>