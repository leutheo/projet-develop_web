<?php
// Modifie les entrées choisies
if (isset($_POST["nommodifier"]) && !empty($_POST["nommodifier"])  
  && $_POST["nommodifier"] != $newname["nom"]) {
    try {
      $nouveaunewname = trim(htmlspecialchars($_POST["nommodifier"]));
      $insertnewname = $bdd->prepare("UPDATE membres SET nom = ? WHERE id = ?");
      $insertnewname->execute(array($nouveaunewname, $_SESSION["id"]));
      header("Location: profil.php?id=".$_SESSION["id"]);
    }catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  if (isset($_POST["prenommodifier"]) && !empty($_POST["prenommodifier"])  
  && $_POST["prenommodifier"] != $newname["prenom"]) {
    try {
      $nouveaunewprenom = trim(htmlspecialchars($_POST["prenommodifier"]));
      $insertnewprenom = $bdd->prepare("UPDATE membres SET prenom = ? WHERE id = ?");
      $insertnewprenom->execute(array($nouveaunewprenom, $_SESSION["id"]));
      header("Location: profil.php?id=".$_SESSION["id"]);
    }catch (PDOException $e) {
      return $e->getMessage();
    }
  }

  if (isset($_POST["courrielmodifier"]) && !empty($_POST["courrielmodifier"])  
  && $_POST["courrielmodifier"] != $newname["courriel"]) {
    try {
      $nouveaunewcourriel = trim(htmlspecialchars($_POST["courrielmodifier"]));
      $insertnewcourriel = $bdd->prepare("UPDATE membres SET courriel = ? WHERE id = ?");
      $insertnewcourriel->execute(array($nouveaunewcourriel, $_SESSION["id"]));
      header("Location: profil.php?id=".$_SESSION["id"]);
    }catch (PDOException $e) {
      return $e->getMessage();
    }
    
  }

  if (isset($_POST["numeromodifier"]) && !empty($_POST["numeromodifier"])  
  && $_POST["numeromodifier"] != $newname["numero"]) {
    try {
      $nouveaunewnumero = trim(htmlspecialchars($_POST["numeromodifier"]));
      $insertnewnumero = $bdd->prepare("UPDATE membres SET numero = ? WHERE id = ?");
      $insertnewnumero->execute(array($nouveaunewnumero, $_SESSION["id"]));
      header("Location: profil.php?id=".$_SESSION["id"]);
    }catch (PDOException $e) {
      return $e->getMessage();
    }  
  }

  if (isset($_POST["naissancemodifier"]) && !empty($_POST["naissancemodifier"])  
  && $_POST["naissancemodifier"] != $newname["naissance"]) {
    try {
      $nouveaunewnaissance = trim(htmlspecialchars($_POST["naissancemodifier"]));
      $insertnewnaissance = $bdd->prepare("UPDATE membres SET naissance = ? WHERE id = ?");
      $insertnewnaissance->execute(array($nouveaunewnaissance, $_SESSION["id"]));
      header("Location: profil.php?id=".$_SESSION["id"]);
    }catch (PDOException $e) {
      return $e->getMessage();
    }  
  }

  if (isset($_POST["utilisateurmodifier"]) && !empty($_POST["utilisateurmodifier"])  
  && $_POST["utilisateurmodifier"] != $newname["utilisateur"]) {
    try {
      $nouveaunewutilisateur = trim(htmlspecialchars($_POST["utilisateurmodifier"]));
      $insertnewutilisateur = $bdd->prepare("UPDATE membres SET utilisateur = ? WHERE id = ?");
      $insertnewutilisateur->execute(array($nouveaunewutilisateur, $_SESSION["id"]));
      header("Location: profil.php?id=".$_SESSION["id"]);
    }catch (PDOException $e) {
      return $e->getMessage();
    }
    
  }

  if (isset($_POST["passmodifier"]) && !empty($_POST["passmodifier"]) 
  && isset($_POST["confirmpassmodifier"]) && !empty($_POST["confirmpassmodifier"])) {
    
      $motpass1 = sha1($_POST["passmodifier"]);
      $motpass2 = sha1($_POST["confirmpassmodifier"]);
    
  if ($motpass1 == $motpass2) {
    try {
      $insertmotpass = $bdd->prepare("UPDATE membres SET pass = ? WHERE id = ?");
      $insertmotpass->execute(array($motpass1, $_SESSION["id"]));
      header("Location: profil.php?id=".$_SESSION["id"]);
    }catch (PDOException $e) {
      return $e->getMessage();
    }
    
  }else {
    $erreurpass = "Vos de passe ne correspondent pas !";
  }
}

include_once'photo-function.php';

// Renvoies vers la page du profil
// if (isset($_POST["nommodifier"]) && $_POST["nommodifier"] == $newname["nom"]) {

//   header("Location: profil.php?id=".$_SESSION["id"]);

//}  
?>