<?php
    // Appelle la base des données
    include_once'./includes/functions/data/connecteur.php';

    if (isset($_POST["formlogin"])) {
            // Vérification de connexion d'un utilisateur
            $userlogin = trim(htmlspecialchars($_POST["userlogin"]));
            $passlogin = trim(sha1($_POST["passlogin"]));
        
        if (!empty($userlogin) && !empty($passlogin)) {
            //Connexion à la base des données
            try {
                $requserlogin = $bdd->prepare("SELECT * FROM membres WHERE utilisateur = ? && pass = ?");
                $requserlogin->execute(array($userlogin, $passlogin));
                $userloginexiste = $requserlogin->rowCount();
            } catch (PDOException $e) {
                return $e->getMessage();
            }

            // Test pour si un utilisateur est connecté
            if ($userloginexiste == 1) {
                try {
                    $userconnect = $requserlogin->fetch();
                    $_SESSION["id"] = $userconnect["id"];
                    $_SESSION["userlogin"] = $userconnect["userlogin"];
                    $_SESSION["passlogin"] = $userconnect["passlogin"];
                    
                    // redirection vers la page profil lorsque l'utilisateur est connecté
                    header("Location: profil.php?id=".$_SESSION["id"]);
                } catch (PDOException $e) {
                    return $e->getMessage();
                }
            }else {
                $erreurlogin = "Utilisateur et mot de passe erronés !";
            }

        }else {
            $erreurlogin = "Tous les champs doivent être complétés !";
        }
    }


?>