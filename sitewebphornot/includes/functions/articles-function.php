<?php
    // Appelle la base des données
    include_once'./includes/functions/data/connecteur.php';

    if(isset($_GET['id']) AND !empty($_GET['id'])) {
        $get_id = htmlspecialchars($_GET['id']);

        $article = $bdd->prepare('SELECT * FROM articles WHERE id = ?');
        $article->execute(array($get_id));
        
        if($article->rowCount() == 1) {
            $article = $article->fetch();
            $titre = $article['titre'];
            $contenu = $article['contenu'];
            $date = $article['date_time_publication'];
        } else {
            die('L\'article que vous souhaitez n\'existe pas !');
        }
    } else {
        die('Oups Erreur !');
    }

?>