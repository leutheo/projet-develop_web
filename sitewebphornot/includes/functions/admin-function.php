<?php
    // Appelle la base des données
    include_once'./includes/functions/data/connecteur.php';

    if (isset($_POST["formconnexion"])) {
            // Vérification de connexion d'un utilisateur
            $adminconnexion = trim(htmlspecialchars($_POST["adminconnexion"]));
            $passadmin = trim(sha1($_POST["passadmin"]));
        
        if (!empty($adminconnexion) && !empty($passadmin)) {
            //Connexion à la base des données
            try {
                $reqadminconnexion = $bdd->prepare("SELECT * FROM membres WHERE utilisateur = ? && pass = ? && administrateur = '1'" );
                $reqadminconnexion->execute(array($adminconnexion, $passadmin));
                $adminconnexionexiste = $reqadminconnexion->rowCount();
            } catch (PDOException $e) {
                return $e->getMessage();
            }

            // Test pour si un utilisateur est connecté comme  administrateur
            if ($adminconnexionexiste == 1 ) {
                try {

                    $adminconnect = $reqadminconnexion->fetch();
                    $_SESSION["id"] = $adminconnect["id"];
                    $_SESSION["adminconnexion"] = $adminconnect["adminconnexion"];
                    $_SESSION["passadmin"] = $adminconnect["passadmin"];
                    
                    header("Location: blogadmin.php");
                } catch (PDOException $e) {
                    return $e->getMessage();
                }
            }else {
                $erreuradmin = "Administrateur et mot de passe erronés !";
            }

        }else {
            $erreuradmin = "Tous les champs doivent être complétés !";
        }
    }
?>