<?php
    // Photo de profil
if (isset($_FILES["photo"]) && !empty($_FILES["photo"]["name"])) {

    $taillemax = 2097152;
    $extension = array("jpg", "jpeg", "gif", "png");
  
    if ($_FILES["photo"]["size"] <= $taillemax){
  
      $extensionChargee = substr(strrchr($_FILES["photo"]["name"], "."), 1);
  
      if (in_array($extensionChargee, $extension)) {
        $chemin = "image/".$_SESSION["id"].".".$extensionChargee;
        $resultat = move_uploaded_file($_FILES["photo"]["tmp_name"], $chemin);
  
        if ($resultat) {
          try {
            $updatephoto = $bdd->prepare("UPDATE membres SET photo = :photo WHERE id = :id");
            $updatephoto->execute(array(
                          "photo" => $_SESSION["id"].".".$extensionChargee,
                          "id"    => $_SESSION["id"]));
            header("Location: profil.php?id=".$_SESSION["id"]);
          } catch (PDOException $e) {
            return $e->getMessage();
          }
        }else {
          $erreurphoto = "Erreur durant l'importation de la photo";
        }
      }else {
        $erreurphoto = "Votre photo de profil doit être au format jpg, jpeg, gif ou png";
      }
    }else {
      $erreurphoto = "Votre photo de profil ne doit pas dépasser 2Mo";
    }
  }
?>