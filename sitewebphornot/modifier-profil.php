<?php
  include_once'./includes/functions/data/connecteur.php';
  include_once'./includes/parts/header.php';  
  include_once'./includes/parts/menu-header.php';
?>

<?php
// Affiche la page si l'utilisateur existe
if (isset($_SESSION["id"])) {
  try {
    $reqnewname = $bdd->prepare("SELECT * FROM membres WHERE id = ?");
    $reqnewname->execute(array($_SESSION["id"]));
    $newname = $reqnewname->fetch();
  } catch (PDOException $e) {
    return $e->getMessage();
  }
  
  //appelle page modifier-profil-functions
  include_once'./includes/functions/modifier-profil-functions.php';

  ?>

  <main id="container">

  <section id="hero-banner">
    <?php
      include_once'./includes/parts/login.php';
    ?>
    <h2> Modification de Profil du membre <?php echo $newname["nom"];?> </h2>

  </section>

  <section id="champion">
    <form id="form-modification" action="" method="POST" enctype="multipart/form-data">
      <fieldset class="reinitialise">
        <legend>Identité du membre</legend>
        <p>
          <label for="nommodifier">Nom :</label>
          <input class="inputLargeur" type="text" name="nommodifier" id="nommodifier" placeholder="nom" value="<?php echo $newname["nom"];?>">
        </p>
        <p>
          <label for="prenommodifier">Prénom :</label>
          <input class="inputLargeur" type="text" name="prenommodifier" id="prenommodifier" placeholder="prénom" value="<?php echo $newname["prenom"];?>">
        </p>
        <p>
          <label for="courrielmodifier">Courriel :</label>
          <input class="inputLargeur" type="email" name="courrielmodifier" id="courrielmodifier" placeholder="example@mail.com" value="<?php echo $newname["courriel"];?>">
        </p>
        <p>
          <label for="numeromodifier">Numéro de téléphone :</label>
          <input class="inputLargeur" type="tel" name="numeromodifier" id="numeromodifier" placeholder="(000)-000-0000" value="<?php echo $newname["numero"];?>">
        </p>
        <p>
          <label for="naissancemodifier">Date de naissance :</label>
          <input class="inputLargeur" type="date" name="naissancemodifier" id="naissancemodifier" value="<?php echo $newname["naissance"];?>">
        </p>
        <p>
            <label for="utilisateur">Nom d'utilisateur :</label>
            <input class="inputLargeur" type="text" id="utilisateurmodifier" name="utilisateurmodifier" placeholder="nom utilisateur" value="<?php echo $newname["utilisateur"];?>">
        </p>
        <p>
          <label for="psw3modifier">Insérez votre photo :</label>
          <input class="inputLargeur" type="file" name="photo" placeholder="Choisissez votre photo de profile" >
        </p>
        <p>
          <label for="psw3modifier">Mot de passe :</label>
          <input class="inputLargeur" type="password" id="passmodifier" name="passmodifier" placeholder="mot de passe" >
        </p>
        <p>
          <label for="psw3modifier">Confirmation mot de passe :</label>
          <input class="inputLargeur" type="password" id="confirmpassmodifier" name="confirmpassmodifier" placeholder="mot de passe" >
        </p>

          </fieldset>
          <input class="bouton btn" type="reset" value="Annuler" >
          <input class="bouton fr btn" type="submit" name="profilmodifier" value="Mise à jour du profil !">
      </form>
      <?php 
      
        if (isset($erreurpass)) {
          echo $erreurpass;
        }

      ?>
  </section>

<section>
<?php
  if(isset($erreurlogin) && !empty($erreurlogin)){
?>
  <h2>Gestion des erreurs du formulaire</h2>
   <?php
    echo $erreurlogin;   
   ?> 
</section>

</main>

<?php
  }  
?>
<?php
  }else {
    // redirection vers la page inscrption
    header("Location: inscription.php");
  }  
?>
<?php
  include_once'./includes/parts/footer.php';
?>