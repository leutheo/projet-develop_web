
<?php
  include_once'./includes/parts/header.php';  
  include_once'./includes/parts/menu-header.php';
?>
  
  <main id="container">

    <section id="hero-banner">
      <h2>Création article</h2>
    </section>


    <section class="container">
    <?php
        include_once'./includes/functions/creation-article_function.php';
      ?>
      <form action="" method="POST">
        <input type="text" placeholder="Titre" name="article_titre" <?php if ($mode_edition == 1) { ?>
          value="<?php echo $edit_article["titre"] ?>"<?php } ?> /></br>
        <textarea name="article_contenu" id="contenu" col="30" rows="10" placeholder="Contenu de l'article"> <?php if ($mode_edition == 1) { ?> 
        <?php echo $edit_article["contenu"] ?>  <?php }?> </textarea></br>
        <input type="submit" value="Envoyer l'article">
      </form></br>

      <?php
        if (isset($erreurarticle)) {
        echo $erreurarticle;
        } 
      ?>
    </section>
  </main>

<?php
  include_once'./includes/parts/footer.php';
?>