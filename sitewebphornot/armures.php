<?php
    include_once'./includes/functions/inscription-functions.php';
    include_once'./includes/parts/header.php';  
    include_once'./includes/parts/menu-header.php'; 
?>

<main id="container">
    <section id="hero-banner">
        <?php
            include_once'./includes/parts/login.php';
        ?>
        <h1>Armures</h1>
    </section>

    <div id="lamodal" class="modal">
        <img class="armures" src="./img/personnage.jpg" alt="personnage">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, error officiis ullam nesciunt consequatur fugiat incidunt, animi dolores quaerat modi provident tenetur et temporibus itaque quod labore autem, repellat impedit.</p>
        <a href="#" rel="modal:close">Ok</a>
    </div>

    <section id="galleryArmures">
        <img id="gallery" src="img/personnage.jpg" alt="">
        </br>
        </br>
        <div id="armures">
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/personnage.jpg" alt="">
            </a>
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/logo1.png" alt="">
            </a>
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/logo2.png" alt="">
            </a>
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/logo.png" alt="">
            </a>
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/personnage.jpg" alt="">
            </a>
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/personnage.jpg" alt="">
            </a>
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/personnage.jpg" alt="">
            </a>
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/personnage.jpg" alt="">
            </a>
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/personnage.jpg" alt="">
            </a>
            <a href="#lamodal" rel="modal:open">
                <img class="miniature" src="img/personnage.jpg" alt="">
            </a>
            
        </div>
    </section>
</main>

<?php
    include_once'./includes/parts/footer.php';
?>