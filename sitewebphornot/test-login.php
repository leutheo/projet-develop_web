  <?php
    include_once'./includes/parts/header.php';  
    include_once'./includes/parts/menu-header.php';  
  ?>



<main id="container">
    <section id="hero-banner">

    <form id="login" action="">
            <p class="formspace">
              <label for="user">Nom d'utilisateur :</label>
              <input class="inputLargeur" type="text" id="user" name="user" placeholder="nom utilisateur" >
            </p>
            <p class="formspace">
              <label for="psw">Mot de passe :</label>
              <input class="inputLargeur" type="psw" id="psw" name="psw" placeholder="mot de passe" >
            </p>
            <input class="bouton fr btn formspace btnwidth" type="submit" value="Login">
      </form>

      <h2>Inscrivez-vous ! </br> C'est rapide et facile</h2>

    </section>
    <?php 
      include_once'./includes/parts/banniere.php';
    ?>

<?php
    $bdd = new PDO('mysql:host=127.0.0.1; dbname=sitewebphornot', 'root','');

    if(isset($_POST['inscription'])){
      $nom = htmlspecialchars($_POST['nom']);
      $prenom = htmlspecialchars($_POST['prenom']);
      $courriel = htmlspecialchars($_POST['courriel']);
      $numero = htmlspecialchars($_POST['numero']);
      $utilisateur = htmlspecialchars($_POST['utilisateur']);
      $pass = sha1($_POST['pass']);
      $confirmpassword = sha1($_POST['confirmpassword']);

      if(!empty($_POST['nom']) AND !empty($_POST['prenom']) AND !empty($_POST['courriel']) AND !empty($_POST['numero']) 
       AND !empty($_POST['utilisateur']) AND !empty($_POST['pass']) AND !empty($_POST['confirmpassword'])){
         
          $nomlength = strlen($nom);

          if($nomlength <= 255)
          {
            $reqpass = $bdd->prepare("SELECT*FROM membres WHERE pass = ?");
            $reqpass->execute(array($pass));
            $passexist = $reqpass->rowCount();
            if($passexist == 0){
              if ($pass == $confirmpassword) {
                $insertmbr = $bdd->prepare("INSERT INTO membres(nom, prenom, email, telephone, utilisateur, pass) VALUE (?, ?, ?, ?, ?, ?)");
                $insertmbr->execute(array($nom, $prenom, $courriel, $numero, $utilisateur, $pass));
                $erreur = "votre compte a bien été crée ! <a href=\"test-login.php\">Me connecter</a>";
                // header('Location: index.php');
              }else{
                $erreur = "Vos mots de passe ne sont pas identiques";
              }
            }else{
              $erreur = "Votre mot de passe est déjà utilisé";
            }
              
          }else{
            $erreur = "Votre pseudo ne peut pas être supérieurrieur à 255 caractères";
          }
      }else{
        $erreur = "Tous les champs ne sont pas remplis";
      }
    }
  ?>
      <section>
      <h2 class="form">Formulaire d'inscription</h2>
      <form id="form-inscription" action="" method="POST">
        <fieldset>
          <legend>Informations personnelles</legend>
          <p>
            <label for="nom">Votre nom :</label>
            <input class="inputLargeur" type="text" name="nom" id="nom" placeholder="nom" value="<?php if(isset($nom)) {echo $nom;}?>" >
          </p>
          <p>
            <label for="prenom">Votre prénom :</label>
            <input class="inputLargeur" type="text" name="prenom" id="prenom" placeholder="prénom" value="<?php if(isset($prenom)) {echo $prenom;}?>">
          </p>
          <p>
            <label for="courriel">Votre courriel :</label>
            <input class="inputLargeur" type="email" name="courriel" id="courriel" placeholder="example@mail.com" value="<?php if(isset($courriel)) {echo $courriel;}?>">
          </p>
          <p>
            <label for="numero">Votre numéro de téléphone :</label>
            <input class="inputLargeur" type="tel" name="numero" id="numero" placeholder="(000)-000-0000" value="<?php if(isset($numero)) {echo $numero;}?>">
          </p>
        
        </fieldset>
        <fieldset>
          <legend>Création de compte</legend>
          <p>
            <label for="utilisateur">Entrez un nom d'utilisateur :</label>
            <input class="inputLargeur" type="text" id="utilisateur" name="utilisateur" placeholder="nom utilisateur" value="<?php if(isset($utilisateur)) {echo $utilisateur;}?>">
          </p>
          <p>
            <label for="psw1">Entrez un mot de passe :</label>
            <input class="inputLargeur" type="password" id="pass" name="pass" placeholder="mot de passe" value="<?php if(isset($pass)) {echo $pass;}?>">
          </p>
          <p>
            <label for="psw2">Confirmez le mot de passe :</label>
            <input class="inputLargeur" type="password" id="confirmpassword" name="confirmpassword" placeholder="confirmez mot de passe" value="<?php if(isset($confirmpassword)) {echo $confirmpassword;}?>" >
          </p>
        </fieldset>          
        <input class="bouton btn" type="reset" value="Effacer le formulaire" >
        <input class="bouton fr btn" type="submit" name="inscription" value="Soumettre votre formulaire">
      </form>
      <?php
        if(isset($erreur)){
          echo $erreur;
        }
      ?>
      </section>
</main>

  <?php
    include_once'./includes/parts/footer.php';
  ?>
 