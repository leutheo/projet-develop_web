
<?php
  include_once'./includes/parts/header.php';  
  include_once'./includes/parts/menu-header.php';
?>
  
  <main id="container">

    <section id="hero-banner">
      <h2>Administration du site</h2>
    </section>

    <section id="logine">
      <?php
        include_once'./includes/functions/admin-function.php';
      ?>
      <form id="login" action="" method="POST">
        <p class="formspace">
          <label for="utilisateur">Nom d'utilisateur :</label>
          <input class="inputLargeur" type="text" id="adminconnexion" name="adminconnexion" placeholder="nom utilisateur" >
        </p>
        <p class="formspace">
          <label for="pass">Mot de passe :</label>
          <input class="inputLargeur" type="password" id="passadmin" name="passadmin" placeholder="mot de passe" >
        </p>
        <input class="bouton fr btn formspace btnwidth" type="submit" name="formconnexion" value="Connexion">
      </form>
      <?php
        if(isset($erreuradmin)){
            echo $erreuradmin;  
        } 
        ?>
      </section>
  </main>

<?php
  include_once'./includes/parts/footer.php';
?>