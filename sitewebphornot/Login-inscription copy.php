  <?php
    include_once'./includes/functions/creationcompte-function.php';
    include_once'./includes/parts/header.php';  
    include_once'./includes/parts/menu-header.php';

    if(!empty($errors)){
      var_dump($errors);
    }  
  ?>

<main id="container">
    <section id="hero-banner">

    <form id="login" action="">
            <p class="formspace">
              <label for="user">Nom d'utilisateur :</label>
              <input class="inputLargeur" type="text" id="user" name="user" placeholder="nom utilisateur" value="<?php form_values("user") ?>">
            </p>
            <p class="formspace">
              <label for="psw">Mot de passe :</label>
              <input class="inputLargeur" type="psw" id="psw" name="psw" placeholder="mot de passe" value="<?php form_values("mpsw") ?>">
            </p>
            <input class="bouton fr btn formspace btnwidth" type="submit" value="Login">
      </form>

      <h2>Inscrivez-vous ! </br> C'est rapide et facile</h2>

    </section>
    <?php 
      include_once'./includes/parts/banniere.php';
    ?>
      <section>
      <h2 class="form">Formulaire d'inscription</h2>
      <form id="form-inscription" action="/sitewebphornot/Login-inscription.php" method="POST">
        <fieldset>
          <legend>Création de compte</legend>
          <!-- <p>
            <label>Identification :</label>
            <input type="radio" name="identification" id="radMme" value="<?php form_values("identification") ?>">
            <label for="radMme">Mme</label>
            <input type="radio" name="identification" id="radM" value="<?php form_values("identification") ?>">
            <label for="radM">M</label>
          </p> -->
          <p>
            <label for="nom">Nom :</label>
            <input class="inputLargeur" type="text" name="nom" id="nom" placeholder="nom" value="<?php form_values("nom") ?>" >
          </p>
          <p>
            <label for="prenom">Prénom :</label>
            <input class="inputLargeur" type="text" name="prenom" id="prenom" placeholder="prénom" value="<?php form_values("prenom") ?>">
          </p>
          <p>
            <label for="courriel">Courriel :</label>
            <input class="inputLargeur" type="email" name="courriel" id="courriel" placeholder="example@mail.com" value="<?php form_values("courriel") ?>">
          </p>
          <p>
            <label for="numero">Téléphone :</label>
            <input class="inputLargeur" type="tel" name="numero" id="numero" placeholder="(000)-000-0000" value="<?php form_values("numero") ?>">
          </p>
          <p>
            <label for="naissance">Date de naissance :</label>
            <input class="inputLargeur" type="date" name="naissance" id="naissance" value="<?php form_values("naissance") ?>" max="2019-12-31">
          </p>
          <p>
            <label for="utilisateur">Nom d'utilisateur :</label>
            <input class="inputLargeur" type="text" id="utilisateur" name="utilisateur" placeholder="nom utilisateur" value="<?php form_values("utilisateur") ?>">
          </p>
          <p>
            <label for="psw1">Mot de passe :</label>
            <input class="inputLargeur" type="password" id="password" name="password" placeholder="mot de passe" value="<?php form_values("mpassword") ?>">
          </p>
          <p>
            <label for="psw2">Confirmation mot de passe :</label>
            <input class="inputLargeur" type="password" id="confirmpassword" name="confirmpassword" placeholder="confirmez mot de passe" value="<?php form_values("confirmpassword") ?>">
          </p>
        </fieldset>
        <!--<fieldset>
          <legend>Création de compte</legend>
           <p>
            <input type="checkbox" onclick="champs_compte()" name="ckbCompte" id="ckbCompte" checked>
            <label for="ckbCompte">Je veux créer mon compte pour les prochaines utilisations</label> 
          </p> 
        </fieldset>  -->        
        <input class="bouton btn" type="reset" value="Effacer le formulaire" >
        <input class="bouton fr btn" type="submit" value="Soumettre votre formulaire">
      </form>
      </section>
  <?php
    if(isset($erreur)){
      echo $erreur;  
    // if(isset($errors) && !empty($errors)){
  ?>
    <section>
      <h1>Gestion des erreurs du formulaire</h1>
      <div>
        <?php
          // foreach ($errors as $error){
          //   echo "<div". $errors . "</div>";
          // }
        ?>
      </div>
    </section>

</main>

<?php
   }
?>
  <?php
    include_once'./includes/parts/footer.php';
  ?>
 