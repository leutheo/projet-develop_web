
<?php
  include_once'./includes/parts/header.php';  
  include_once'./includes/parts/menu-header.php';
?>
  
  <main id="container">

    <section id="hero-banner">
      <h2>Blog</h2>
    </section>


    <section class="container">
    <?php
        include_once'./includes/functions/blog_function.php';
      ?>

      <ul>
        <?php while($a = $articles->fetch()){ ?>
          <li class="txt-white">
            <a class="txt-white" href="articles.php?id=<?php echo $a['id'] ?>"><?php echo $a['titre'] ?></a> | 
            <a class="txt-white" href="creation-article.php?edit=<?php echo $a['id'] ?>">Modifier</a> | 
            <a class="txt-white" href="supprimer.php?id=<?php echo $a['id'] ?>">Supprimer</a>
          </li>
        <?php } ?>
      </ul>
      <button class="btn"><a class="txt-white" href="creation-article.php">Création article</a></button>
    </section>
  </main>

<?php
  include_once'./includes/parts/footer.php';
?>