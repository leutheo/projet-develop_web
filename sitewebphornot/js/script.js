// Animation menu
$("#main-nav ul li ").hover(function() {
  $(this).fadeToggle(3000);
});

// fonction appelle menumobile
$("#activate_menu").on('click',function(e){
  $(this).next("div").toggleClass("show");
});

//gallery armes animée
$(document).ready(function(){
  $("#armes img").on({
    mouseover: function() {
      $(this).css({
        "cursor": "pointer",
        "border-color" : "blue"
      });
    },
    mouseout: function() {
      $(this).css({
        "cursor": "default",
        "border-color" : "yellow"
      });
    },
    click: function(){
      var imageMiniature = $(this).attr("src");
      $("#gallery").fadeOut(300, function(){
        $(this).attr("src", imageMiniature);
      }).fadeIn(300);
    }
  });
});


//gallery armes animée
$(document).ready(function(){
  $("#armures img").on({
    mouseover: function() {
      $(this).css({
        "cursor": "pointer",
        "border-color" : "blue"
      });
    },
    mouseout: function() {
      $(this).css({
        "cursor": "default",
        "border-color" : "yellow"
      });
    },
    click: function(){
      var imageMiniature = $(this).attr("src");
      $("#gallery").fadeOut(300, function(){
        $(this).attr("src", imageMiniature);
      }).fadeIn(300);
    }
  });
});

// animation armures et armes
$(function (e) {
  $(".armures, .armes" ).hover(function () {
    $(this).width(0.8*$(this).width());
    $(this).height(0.8*$(this).height());
  },function () {
    $(this).width(1.25*$(this).width());
    $(this).height(1.25*$(this).height()); 
  });
});


// Animation avatr profil
$(function() {
  $('#avatar').animate(
    { deg: 360 },
    {
      duration: 1200,
      step: function(now) {
        $('#avatar').css({ transform: 'rotate(' + now + 'deg)' });
      }
    }
  );
});

// affichage de champion
$(function() {
  $(function(){
  $('.titre',"#avatar").slideToggle('slow');
}, function(){
  $('.titre',"#avatar").slideToggle('slow');
  });
});

setTimeout( function(){$(".titre").hide();} , 0);
setTimeout( function(){$(".titre ").show();}, 3000);

// Animation logo
setTimeout( function(){$("#site-logo").hide();} , 0);
setTimeout( function(){$("#site-logo ").show();}, 1000);

$(function() {
  $('#site-logo').animate({
    marginLeft: '50px'
  }, 1000);
});


$(function() {
  doBounce($("#site-logo"), 5, '20px', 300);   
});


function doBounce(element, times, distance, speed) {
  for(i = 0; i < times; i++) {
      element.animate({marginTop: '-='+distance},speed)
          .animate({marginTop: '+='+distance},speed);
  }        
}

$(function() {
  $('#site-logo').animate({
    marginLeft: '0px'
  }, 1000);
});

// Animation pub
var path_hg = anime.path("#trajectoire #hautgauche");

anime({
  targets: '#premier',
  translateX: path_hg('x'),
  translateY: path_hg('y'),
//   rotate: path('angle'),
  easing: 'linear',
  duration: 4000,
//   loop: true
});

var path_hd = anime.path("#trajectoire #hautdroit");

anime({
  targets: '#rouge',
  translateX: path_hd('x'),
  translateY: path_hd('y'),
//   rotate: path_hd('angle'),
  easing: 'linear',
  duration: 4000,
//   loop: true
});


var path_bg = anime.path("#trajectoire #basgauche");

anime({
  targets: '#bleu',
  translateX: path_bg('x'),
  translateY: path_bg('y'),
//   rotate: path_bg('angle'),
  easing: 'linear',
  duration: 4000,
//   loop: true
});

var path1 = anime.path("#trajectoire #basdroit");

anime({
  targets: '#deuxieme',
  translateX: path1('x'),
  translateY: path1('y'),
//   rotate: path('angle'),
  easing: 'linear',
  duration: 4000,
  
//   loop: true
});

setTimeout( function(){$("#vs").hide();} , 0);
setTimeout( function(){$("#vs ").show();}, 4500);


//menumobile
// Close the dropdown if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}
