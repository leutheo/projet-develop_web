<?php
  include_once'./includes/functions/inscription-functions.php';
  include_once'./includes/parts/header.php';  
  include_once'./includes/parts/menu-header.php';
  if(!empty($errors)){
    var_dump($errors);
  }
?>
  
<main id="container">
  <section id="hero-banner">
  <?php
    include_once'./includes/parts/login.php';
  ?>
    <h1>Statistiques du combattant</h1>
  </section>
<?php
  if (isset($_SESSION["id"]) && $_SESSION["id"] > 0) {
    include_once'./includes/functions/profil-functions.php';  
?>

<section id="champion" class="flex">
  <div class="champion-cards punch stat">
  <?php 
    if (!empty($userconnect["photo"])) {
  ?>
    <div class="profil">
      <img id="avatar" src="image/<?php echo $userconnect["photo"]; ?>" alt="champion" width="300" max-height="300"/>
      <h2 class="titre">champion !!!</h2>
    </div>
  <?php
    }
  ?>
  </div>
  <div class="champion-cards edit identite">
    <h2>Identité</h2>
      <p class="txt-white ">
        Nom : <?php echo $userconnect["nom"]; ?>
      </p>
      <p class="txt-white ">
        Prénom : <?php echo $userconnect["prenom"]; ?>
      </p>
      <p class="txt-white ">
        Date de naissance : <?php echo $userconnect["naissance"]; ?>
      </p>
      <p class="txt-white ">
        Courriel : <?php echo $userconnect["courriel"]; ?>
      </p>
      <p class="txt-white ">
        Téléphone : <?php echo $userconnect["numero"]; ?>
      </p>
  </div>
  <div class="champion-cards edit">
    <h2>Statistiques</h2>
    <p class="txt-white ">
      Discuté : <?php echo $userconnect["combat"]; ?>
    </p>
    <p class="txt-white">
      Gagné : <?php echo $userconnect["gagne"]; ?>
    </p>
    <p class="txt-white">
      Perdu : <?php echo $userconnect["perdu"]; ?>
    </p>
    <p></p>
    <p></p>
  </div>
<?php  
  }
?>

</section>

    <?php
    if(isset($errors) && !empty($errors)){
  ?>
    <section>
      <h1>Gestion des erreurs du formulaire</h1>
      <div>
        <?php
          foreach ($errors as $error){
            echo "<div". $errors . "</div>";
          }
        ?>
      </div>
    </section>

</main>

<?php
   }
?>

<?php
  include_once'./includes/parts/footer.php';
?>