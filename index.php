<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <title>Intro PHP</title>
</head>
<body>

    
    <?php 
    /*
       $tableau_vide = [];
       $tableau_plein = ["a", 2, 3.3, "b"];
       $utilisateur = [
            ["nom" => "Waganwheel", "prenom" => "Jean", "comment" =>"C'est hat les tableaux"],
            ["nom" => "Waganwheel", "prenom" => "Jan", "comment" =>"C'est hyt les tableaux"],
            ["nom" => "Waganwheel", "prenom" => "Jen", "comment" =>"C'est hit les tableaux"],
            ["nom" => "Waganwheel", "prenom" => "Jun", "comment" =>"C'est hot les tableaux"],
            ["nom" => "Waganwheel", "prenom" => "Jon", "comment" =>"C'est hut les tableaux"],
            ["nom" => "Waganwheel", "prenom" => "Juy", "comment" =>"C'est het les tableaux"]

       ];
    ?>

    <?php
        foreach($utilisateur as $user) {
    ?>
        <h5>Nom complet : <?php echo $user["prenom"];?>
                        <?php echo $user["nom"];?>
        </h5>
        <h6>Commentaire : </h6>
        <?php echo $user["comment"];?>
    <?php
        }
    ?>
    <br>
    <br>
    <br>

    <?php
        print_r(count($utilisateur));

        for ($i=0; $i <= count($utilisateur) ; $i++) { 
            print_r($i);
        }*/
    ?>

    <?php
        // $data_one = "données";
        // $data_two = "autres";

        // echo "PHP est simple à comprendre !";

        // echo $data_one . $data_two;

        // print_r($data_one);

        // var_dump($data_one);
    ?> 

    <!-- Méthode GET -->
    <?php
    //Je peux stocker les valeurs de mon formulaire
        $prenom = (isset($_GET['prenom']) ? $_GET['prenom'] : "");
        $nom = (isset($_GET['nom']) ? $_GET['nom'] : "");

    // Ici je fait différente section entre le php et le texte
        if (isset($_GET['prenom']) && isset($_GET['nom'])) {
        ?>
        <h2>Bienvenu
        <?php 
            echo $_GET['prenom'] . $_GET['nom'];
        ?>
        </h2>
        <?php
        }

        /*
        Ici je ne fait que trois echo, n'étant que du texte interprété par le navigateur 
        je peux ecrire du html dans mes chaines de caractères. 
        */
        if (isset($_GET['prenom']) && isset($_GET['nom'])) {
            echo "<h2> Bienvenu";
            echo $_GET['prenom'] . $_GET['nom'];
            echo "</h2>";
        }

        /* Ici je fais la concatenation des valeurs de mon formulaire pour les afficher */
        if (isset($_GET['prenom']) && isset($_GET['nom'])) {
            echo "<h2> Bienvenu" . $_GET['prenom'] . $_GET['nom'] . "<h2>";
        }


        /* 
        Ici j'utilise les "" pour pouvoir interpoler automatiquement 
        les variable dans ma chaine de caractères.
        */
        if (isset($_GET['prenom']) && isset($_GET['nom'])) {
            echo "<h2> Bienvenu $prenom $nom </h2>";
        }

        /* 
        Toutes ces méthodes sont bonnes mais je préfère la dernière 
        Court clair concis efficace. Mais elle n'est pas toujours la méthode la 
        plus appropriée pour générer du html. Selon le cas différentes méthodes 
        seront utilisées. 
        */

    ?>
    <form action="/" method="GET">
        <input type="text" name="prenom" placeholder="Prénom"/><br/><br/>
        <input type="text" name="nom" placeholder="Nom"/><br/>
        <input type="submit" value="soumettre"/>
    </form>
    



    <!-- Méthode POST -->
    <?php
        //Je peux stocker les valeurs de mon formulaire
        $prenom = (isset($_POST['prenom']) ? $_POST['prenom'] : "");
        $nom = (isset($_POST['nom']) ? $_POST['nom'] : "");

        // Ici je fait différente section entre le php et le texte
        if (isset($_POST['prenom']) && isset($_POST['nom'])) {
    ?>
        <h2>Bienvenu
    <?php 
            echo $_POST['prenom'] . $_POST['nom'];
    ?>
        </h2>
        
    <?php
        }

        /*
        Ici je ne fait que trois echo, n'étant que du texte interprété par le navigateur 
        je peux ecrire du html dans mes chaines de caractères. 
        */
        if (isset($_POST['prenom']) && isset($_POST['nom'])) {
            echo "<h2> Bienvenu ";
            echo $_POST['prenom'] . $_POST['nom'];
            echo "</h2>";
        }
        



        /* 
        Ici j'utilise les "" pour pouvoir interpoler automatiquement 
        les variable dans ma chaine de caractères.
        */
        if (isset($_POST['prenom']) && isset($_POST['nom'])) {
            echo "<h2> Bienvenu $prenom $nom </h2>";
        }

        /* 
        Toutes ces méthodes sont bonnes mais je préfère la dernière 
        Court clair concis efficace. Mais elle n'est pas toujours la méthode la 
        plus appropriée pour générer du html. Selon le cas différentes méthodes 
        seront utilisées. 
        */

    ?>
    <br/><br/>
    <form action="/" method="POST">
        <input type="text" name="prenom"/><br/><br/>
        <input type="text" name="nom"/><br/>
        <input type="submit" value="soumettre"/>
    </form>

   <!-- EXERCICE -->
   <?php
    //Je peux stocker les valeurs de mon formulaire
        $prenom = (isset($_GET['prenom']) ? $_GET['prenom'] : "");
        $nom = (isset($_GET['nom']) ? $_GET['nom'] : "");
        $pays = (isset($_GET['pays']) ? $_GET['pays'] : "");
        $email = (isset($_GET['email']) ? $_GET['email'] : "");

    // Ici je fait différente section entre le php et le texte
        if (isset($_GET['prenom']) && isset($_GET['nom']) && isset($_GET['pays']) && isset($_GET['email'])) {
        ?>
        <h1>Bienvenu
        <?php 
            echo $_GET['prenom'] . " " . $_GET['nom'] . " " . "du" ." " .  $_GET['pays'] . " " .  $_GET['email'];
        ?>
        </h1>
        <?php
        }

        /*
        Ici je ne fait que trois echo, n'étant que du texte interprété par le navigateur 
        je peux ecrire du html dans mes chaines de caractères. 
        */
        if (isset($_GET['prenom']) && isset($_GET['nom']) && isset($_GET['pays']) && isset($_GET['email'])) {
            echo "<h1> Bienvenu";
            echo " " . $_GET['prenom'] . " " .  $_GET['nom'] . " " . "du" . " " .  $_GET['pays'] . " " . $_GET['email'];
            echo "</h1>";
        }

        /* Ici je fais la concatenation des valeurs de mon formulaire pour les afficher */
        if (isset($_GET['prenom']) && isset($_GET['nom']) && isset($_GET['pays']) && isset($_GET['email'])) {
            echo " " . "<h1> Bienvenu" . " " . $_GET['prenom'] . " " .  $_GET['nom'] . " " .  "du" . " " .  $_GET['pays'] . " " . $_GET['email'] . "<h1>";
        }


        /* 
        Ici j'utilise les "" pour pouvoir interpoler automatiquement 
        les variable dans ma chaine de caractères.
        */
        if (isset($_GET['prenom']) && isset($_GET['nom']) && isset($_GET['pays']) && isset($_GET['email'])) {
            echo "<h1> Bienvenu $prenom $nom du $pays $email </h1>";
        }

        /* 
        Toutes ces méthodes sont bonnes mais je préfère la dernière 
        Court clair concis efficace. Mais elle n'est pas toujours la méthode la 
        plus appropriée pour générer du html. Selon le cas différentes méthodes 
        seront utilisées. 
        */

    ?>
    <form action="/" method="GET">
        <br/>
        <input type="text" name="prenom" placeholder="Prénom"/><br/><br/>
        <input type="text" name="nom" placeholder="Nom"/><br/><br/>
        <input type="text" name="pays" placeholder="Pays"/><br/><br/>
        <input type="email" name="email" placeholder="email"/><br/><br/>
        <input type="submit" value="soumettre"/>
    </form>
    

    <?php

        $prenom = (isset($_GET['prenom']) ? $_GET['prenom'] : "");
        $nom = (isset($_GET['nom']) ? $_GET['nom'] : "");
        $pays = (isset($_GET['pays']) ? $_GET['pays'] : "");
        $email = (isset($_GET['email']) ? $_GET['email'] : "");
        $maPhrase = (isset($_GET['phrase']) ? $_GET['phrase'] : "");
        
        for ($i=0; $i < 10 ; $i++) { 
            if (isset($_GET['prenom']) && isset($_GET['nom']) && isset($_GET['pays']) && isset($_GET['email']) && isset($_GET['phrase'])) {
                echo "<h1> Bienvenu" . " " . $_GET['prenom'] . " " .  $_GET['nom'] . " " .  "du" . " " .  $_GET['pays'] . " " . $_GET['email'] . " " . $_GET['phrase'] .  "<h1>";
            }
        }
        echo "Notre phrase sera imprimée " . print_r($i) . "fois !";
        
    ?>

    <form action="/" method="GET">
        <br/>
        <input type="text" name="prenom" placeholder="Prénom"/><br/>
        <input type="text" name="nom" placeholder="Nom"/><br/>
        <input type="text" name="pays" placeholder="Pays"/><br/>
        <input type="email" name="email" placeholder="email"/><br/>
        <input type="text" name="phrase" placeholder="Ma phrase"/><br/>
        <input type="submit" value="soumettre"/>
    </form>



    <?php
        
    $voitures = array(
        array(
            "name"=>"Urus", 
            "type"=>"SUV", 
            "brand"=>"Lamborghini"
        ),
        array(
            "name"=>"Cayenne", 
            "type"=>"SUV", 
            "brand"=>"Porsche"
        ),
        array(
            "name"=>"Bentayga", 
            "type"=>"SUV", 
            "brand"=>"Bentley"
        ),
        array(
            "name"=>"Boulemouche", 
            "type"=>"LADA", 
            "brand"=>"Barnakencorenpanne"
        ),
    );
     

    // question 1
    $taille = count($voitures);
        echo "</br>les modèles de voitures sont les suivants : </br>";
    for ($i=0; $i < $taille; $i++) { 
        echo $voitures[$i]["name"] . " " . $voitures[$i]["type"] . " " . $voitures[$i]["brand"] . "</br>";
    }

    // question 2
    $voitureajoutees = array(
        array(
            "name"=>"Rav4",
            "type"=>"SUV",
            "brand"=>"Toyota"
        ),
         array(
            "name"=>"Q8", 
            "type"=>"4x4", 
            "brand"=>"Audi"
            ),
    );
    $merged = array_merge($voitures, $voitureajoutees);

    $taille = count($merged);
        echo "</br>les modèles de voitures sont les suivants : </br>";
    for ($i=0; $i < $taille; $i++) { 
        echo $merged[$i]["name"] . " " . $merged[$i]["type"] . " " . $merged[$i]["brand"] . "</br>";
    }

    // print_r($merged);

    // question 2 suite
  

    echo "question 2 suite </br>";
    //https://stackoverflow.com/questions/6661530/php-multidimensional-array-search-by-value
    $theo = array_search(Bentley, array_column($voitures, 'brand'));
    unset($merged[$theo]);

    // $merged = array_keys($merged);

    $merged = array_values($merged);

    print_r($merged);

    // question 3
    echo "</br>";
    sort($merged);
    
    $taille = count($merged);
    echo "</br>les modèles de voitures sont les suivants : </br>";
    for ($i=0; $i < $taille; $i++) { 
        echo $merged[$i]["name"] . " " . $merged[$i]["type"] . " " . $merged[$i]["brand"] . "</br>";
    }


    // print_r($merged);

    // question 4
    echo "</br>question 4";
    print_r(array_slice($voitures,2, 2));

    // question 5
    echo "</br>question 5";
    for ($i=0; $i < $taille; $i++) { 
        echo str_replace('a','e',$merged[$i]["name"] . " " . $merged[$i]["type"] . " " . $merged[$i]["brand"] . "</br>");
        // for ($j=0; $j < 5 ; $j++) { 
        //     echo str_replace('a','e', $merged[$i][$j]);
        // }
        // echo str_replace("a", "e", $merged[$i][$i]);
        
    }

    // https://stackoverflow.com/questions/30647581/does-str-replace-go-through-multidimensional-arrays

    // question 6
    echo "question 6 </br>";
    // Retournez un tableau avec la taille de tout les valeurs(longueur des mots) additionnés ensemble dans des tableaux 
    // au meme positions que le tableau auquel il fait référence. 

    // echo "longuer totale du tableau : " . sizeof($merged,1);
    // echo "*** Avec 0 on a : ".count($merged, 0). "</br>";
    // echo "*** Avec 1 on a : ".count($merged, 1). "</br>";
    // echo "*** Avec 2 on a : ".count($merged, COUNT_NORMAL). "</br>";
    // echo "*** Avec 3 on a : ".count($merged, COUNT_RECURSIVE). "</br>";
    // echo "*** Avec 4 on a : ".count($merged);
   
    //https://www.php.net/manual/en/function.count.php

    // $sep;
    // function iterate($merged, $sep = ""){
    //     if (is_null($sep)) {
    //         $sep = "||";
    //     } else {
    //         $sep .= "||";
    //     }
    // }
    foreach ($merged as $key => $merge) {
        echo $sep=2;
        if (count($merge)) {
            echo $key. "</br>";
            // iterate($merge,$sep);
        } else {
            echo $key . "</br>";
        }
    }
    // iterate($merged);

    // https://www.w3schools.com/php/func_string_join.asp

    // question 7
    echo "question 7 </br>";
    //Ordonnez le tableau selon le nombre de mot dans les valeurs de 
    // chacune des voitures. ( Donc la voiture ayant le plus de lettre se 
    // retouve en premier et ainsi de suite )


    function triabulle($merged, $n){
        $temp = null;

        for ($i = 0; $i < ($n - 1); $i++)
        {
            for ($j = ($i + 1); $j < $n; $j++)
            {
                if ($merged[$j]["name"] < $merged[$i]["name"])
                {
                    $temp = $merged[$i]; 
                    $merged[$i] = $merged[$j]; 
                    $merged[$j] = $temp;
                }   
            }
    }
    return $merged;
    }
    print_r($merged);
    $n = count($merged);

    $trie = triabulle($merged,$n);
    print_r($trie);

?>


<h1> <u>EXERCICE 1</u> </h1> <br>

<?php
    $cars = array(
        array(
            "name"=>"Urus", 
            "type"=>"SUV", 
            "brand"=>"Lamborghini"
        ),
        array(
            "name"=>"Cayenne", 
            "type"=>"SUV", 
            "brand"=>"Porsche"
        ),
        array(
            "name"=>"Bentayga", 
            "type"=>"SUV", 
            "brand"=>"Bentley"
        ),
        array(
            "name"=>"Boulemouche", 
            "type"=>"LADA", 
            "brand"=>"Barnakencorenpanne"
        ),
    );

    //1
    foreach ($cars as $value) {
        if ($value["type"]=="SUV") {
            echo $value["name"]. "&nbsp;";
            echo $value["brand"]."<br>";
        }
    }
     //2-delete
    $indexBentley;
    foreach ($cars as $key => $value) {
        if ($value["brand"]=="Bentley") {
            $indexBentley=$key;
            echo $key."<br>";
            
        }           
    }

    unset($cars[$indexBentley]);

    //var_dump( $voitures);

    //2-add
    array_push(
        $cars,
        array(
            "name"=>"omar", 
            "type"=>"SUV", 
            "brand"=>"tizi"
        ),
        array(
            "name"=>"mah", 
            "type"=>"LADA", 
            "brand"=>"bejaia"
        ),
    );
    var_dump( $cars);
?>

<?php

echo "</br> </br> </br>";
$input = array("red", "green", "blue", "yellow");
array_splice($input, 2);
var_dump($input);

$input = array("red", "green", "blue", "yellow");
array_splice($input, 1, -1);
var_dump($input);

$input = array("red", "green", "blue", "yellow");
array_splice($input, 1, count($input), "orange");
var_dump($input);

$input = array("red", "green", "blue", "yellow");
array_splice($input, -1, 1, array("black", "maroon"));
var_dump($input);
?>


</body>
</html>