<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ladate' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'vyF999nb6lCK4se+N/>?2[0Rh:<2?dUC1ULkyVFej[+uz8.B)PVmT1RI!uDP+^LR' );
define( 'SECURE_AUTH_KEY',  'AD4eEZS/pM,ld%=eoVvcTZL(J)#U~Hz{umjy8E_H}ehba;^+0Y+>)Lx`B4d3Lc&h' );
define( 'LOGGED_IN_KEY',    '-*#.;w&>&?|pEUYu3s;KcwG24DJyXb{ECyZ+N=g2o6_HemvKf$AtG][ SuJ< ElV' );
define( 'NONCE_KEY',        'xQExh&hIuDc:odF,d#::@O cjznNlBTcr<&990*r{6AE62w(uMv<n3}g#m&+bJS8' );
define( 'AUTH_SALT',        'J]&0MSW zyDRKW.iU//n:h+(f {;MfK?vk.b?$su&Bxjo%T&]FyG{_Lh:}e8$NR-' );
define( 'SECURE_AUTH_SALT', 'mY=NL9s%4uZeLPUo=6W N~[kvV`jMRXWvHf#DX7,V2U3-(rqN!xc5q@S]{WIJMB*' );
define( 'LOGGED_IN_SALT',   '6H0,IKhyF$y@_ZwcTKCikE=lPI?$*:bAVH5{]%S9YwbEVN%}P|#VZZV_A&znG{|.' );
define( 'NONCE_SALT',       'Rv2%.)a]hgsW9{4;]w}k!Nbw!3@vm}z7@I7hqTnDPvm+Bo1Mk;:n>/5cok0X:Yc1' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'markladate_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
